<?php

class Sign_model extends MY_Model
{

    public function valid_user($user_post)
    {
        $q = $this->db->get_where('usuario', $user_post, 1);
        $return = $q->result_array();
        if(empty($return))
        {
            return null;
        }
        else
        {
            return $return[0];
        }
    }

    public function getSessionData($user_id)
    {
        if( isset($user_id) )
        {
            if($user = $this->get_unique('usuario', array('id' => $user_id)))
            {
                return $user;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }


    /*
    public function get($profesor = null)
    {
        $q = $this->db->get_where('profesores', $profesor);
        $return = $q->result_array();
        if(!empty($return))
        {
            return $return;
        }
        else
        {
            return false;
        }
    }

    public function get_profesor($user_id)
    {
        $q = $this->db->get_where('profesores', array('id' => $user_id));
        return $q->result_array();
    }

    // usage: $result = $this->user_model->insert(array('rut' => 'erick', 'password' => md5('sheffer1'), 'email' => 'erick.shuffer@hotmail.com'));
    public function insert($data)
    {
        $this->db->insert('profesores', $data);
        return $this->db->insert_id();
    }

    // usage: $result = $this->user_model->update(array('rut'=>'peggy'),3)
    public function update($data, $user_id)
    {
        $this->db->where(array('id' => $user_id));
        $this->db->update('profesores', $data);
        return $this->db->affected_rows();
    }

    // usage: $result = $this->user_model->delete(3)
    public function delete($user_id)
    {
        $this->db->delete('profesores', array('id'=>$user_id));
        return $this->db->affected_rows();
    }

    public function getUserById($user_id)
    {
        $q = $this->db->get_where('profesores', array('id' => $user_id));
        $return = $q->result_array();
        if(!empty($return))
        {
            return $return[0];
        }
        else
        {
            return false;
        }
    }

    public function getUserByRut($rut)
    {
        $q = $this->db->get_where('profesores', array('rut' => $rut));
        $return = $q->result_array();
        if(!empty($return))
        {
            return $return[0];
        }
        else
        {
            return false;
        }
    }

    public function getUserByEncryptedPassword($passwordEncrypted)
    {
        $q = $this->db->get_where('profesores', array('password' => $passwordEncrypted));
        $return = $q->result_array();
        if(!empty($return))
        {
            return $return[0];
        }
        else
        {
            return false;
        }
    }

    public function redefinePassword($user, $password)
    {
        $this->db->where(array('id' => $user['id']));
        $data = array(
            'password' => hash('sha256', $password.UDP_PASS),
        );
        $this->db->update('profesores', $data);
        return $this->db->affected_rows();
    }
    */

}