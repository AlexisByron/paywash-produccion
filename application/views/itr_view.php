<?php $fecha_actual = date('Y/m/d') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Informes de trabajos realizados</title>

    <!-- vendor css -->
    <link href="<?php echo base_url('assets/lib/@fortawesome/fontawesome-free/css/all.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/ionicons/css/ionicons.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/highlightjs/styles/github.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/select2/css/select2.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/timepicker/jquery.timepicker.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/spectrum-colorpicker/spectrum.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/bootstrap-tagsinput/bootstrap-tagsinput.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/ion-rangeslider/css/ion.rangeSlider.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/lib/ion-rangeslider/css/ion.rangeSlider.skinFlat.css') ?>" rel="stylesheet">
    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bracket.css') ?>">

    <style>
        #row_trabajo_anterior {
            display: none;
        }

        #col_numero_ticket {
            display: none;
        }

        #col_orden_compra {
            display: none;
        }

        #col_contratista_retira_residuos {
            display: none;
        }

        #row_residuos_generados {
            display: none;
        }
    </style>

    <style>
        #preloader {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #fff;
            /* change if the mask should have another color then white */
            z-index: 99;
            /* makes sure it stays on top */
        }

        #status {
            width: 200px;
            height: 200px;
            position: absolute;
            left: 50%;
            /* centers the loading animation horizontally one the screen */
            top: 50%;
            /* centers the loading animation vertically one the screen */
            background-image: url(https://raw.githubusercontent.com/niklausgerber/PreLoadMe/master/img/status.gif);
            /* path to your loading animation */
            background-repeat: no-repeat;
            background-position: center;
            margin: -100px 0 0 -100px;
            /* is width and height divided by two */
        }
    </style>

    <style>
        .btn_descarga{display: none;}
    </style>

</head>

<body>


<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<!-- ########## START: LEFT PANEL ########## -->
<div class="br-logo"><a href=""><span>[</span>Cooprinsem<span>]</span></a></div>
<div class="br-sideleft sideleft-scrollbar">
    <label class="sidebar-label pd-x-10 mg-t-20 op-3">Cooprinsem</label>
    <ul class="br-sideleft-menu">
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub active show-sub">
                <i class="menu-item-icon icon ion-ios-gear-outline tx-24"></i>
                <span class="menu-item-label">Formularios</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item"><a href="<?php echo base_url() ?>" class="sub-link active">ITR</a></li>
                <li class="sub-item"><a href="<?php echo base_url('inicio/administracion') ?>" class="sub-link active">Administración</a></li>
            </ul>
        </li><!-- br-menu-item -->
    </ul><!-- br-sideleft-menu -->

<!--    <label class="sidebar-label pd-x-10 mg-t-25 mg-b-20 tx-info">Resumen de información</label>-->
<!---->
<!--    <div class="info-list">-->
<!--        <div class="info-list-item">-->
<!--            <div>-->
<!--                <p class="info-list-label">Memory Usage</p>-->
<!--                <h5 class="info-list-amount">32.3%</h5>-->
<!--            </div>-->
<!--            <span class="peity-bar"-->
<!--                  data-peity='{ "fill": ["#336490"], "height": 35, "width": 60 }'>8,6,5,9,8,4,9,3,5,9</span>-->
<!--        </div>-->
<!---->
<!--        <div class="info-list-item">-->
<!--            <div>-->
<!--                <p class="info-list-label">CPU Usage</p>-->
<!--                <h5 class="info-list-amount">140.05</h5>-->
<!--            </div>-->
<!--            <span class="peity-bar" data-peity='{ "fill": ["#1C7973"], "height": 35, "width": 60 }'>4,3,5,7,12,10,4,5,11,7</span>-->
<!--        </div>-->
<!---->
<!--        <div class="info-list-item">-->
<!--            <div>-->
<!--                <p class="info-list-label">Disk Usage</p>-->
<!--                <h5 class="info-list-amount">82.02%</h5>-->
<!--            </div>-->
<!--            <span class="peity-bar"-->
<!--                  data-peity='{ "fill": ["#8E4246"], "height": 35, "width": 60 }'>1,2,1,3,2,10,4,12,7</span>-->
<!--        </div>-->
<!---->
<!--        <div class="info-list-item">-->
<!--            <div>-->
<!--                <p class="info-list-label">Daily Traffic</p>-->
<!--                <h5 class="info-list-amount">62,201</h5>-->
<!--            </div>-->
<!--            <span class="peity-bar"-->
<!--                  data-peity='{ "fill": ["#9C7846"], "height": 35, "width": 60 }'>3,12,7,9,2,3,4,5,2</span>-->
<!--        </div>-->
<!--    </div>-->

    <br>
</div><!-- br-sideleft -->
<!-- ########## END: LEFT PANEL ########## -->

<!-- ########## START: HEAD PANEL ########## -->
<div class="br-header">
    <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a>
        </div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a>
        </div>
    </div>

    <div class="br-header-right">
        <nav class="nav">
            <!--
            <div class="dropdown">
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name hidden-md-down">Katherine</span>
                    <img src="https://via.placeholder.com/500" class="wd-32 rounded-circle" alt="">
                    <span class="square-10 bg-success"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-250">
                    <div class="tx-center">
                        <a href=""><img src="https://via.placeholder.com/500" class="wd-80 rounded-circle" alt=""></a>
                        <h6 class="logged-fullname">Katherine P. Lumaad</h6>
                        <p>youremail@domain.com</p>
                    </div>
                    <hr>
                    <div class="tx-center">
                        <span class="profile-earning-label">Earnings After Taxes</span>
                        <h3 class="profile-earning-amount">$13,230 <i class="icon ion-ios-arrow-thin-up tx-success"></i>
                        </h3>
                        <span class="profile-earning-text">Based on list price.</span>
                    </div>
                    <hr>
                    <ul class="list-unstyled user-profile-nav">
                        <li><a href=""><i class="icon ion-ios-person"></i> Edit Profile</a></li>
                        <li><a href=""><i class="icon ion-ios-gear"></i> Settings</a></li>
                        <li><a href=""><i class="icon ion-ios-download"></i> Downloads</a></li>
                        <li><a href=""><i class="icon ion-ios-star"></i> Favorites</a></li>
                        <li><a href=""><i class="icon ion-ios-folder"></i> Collections</a></li>
                        <li><a href=""><i class="icon ion-power"></i> Sign Out</a></li>
                    </ul>
                </div>
            </div>
            -->
        </nav>
    </div>

</div><!-- br-header -->
<!-- ########## END: HEAD PANEL ########## -->

<!-- ########## START: RIGHT PANEL ########## -->
<div class="br-sideright">
    <ul class="nav nav-tabs sidebar-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" role="tab" href="#contacts"><i
                        class="icon ion-ios-contact-outline tx-24"></i></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#attachments"><i
                        class="icon ion-ios-folder-outline tx-22"></i></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#calendar"><i
                        class="icon ion-ios-calendar-outline tx-24"></i></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#settings"><i
                        class="icon ion-ios-gear-outline tx-24"></i></a>
        </li>
    </ul><!-- sidebar-tabs -->

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane pos-absolute a-0 mg-t-60 contact-scrollbar active" id="contacts" role="tabpanel">
            <label class="sidebar-label pd-x-25 mg-t-25">Online Contacts</label>
            <div class="contact-list pd-x-10">
                <a href="" class="contact-list-link new">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-success"></div>
                        </div>
                        <div class="contact-person">
                            <p>Marilyn Tarter</p>
                            <span>Clemson, CA</span>
                        </div>
                        <span class="tx-info tx-12"><span class="square-8 bg-info rounded-circle"></span> 1 new</span>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
                <a href="" class="contact-list-link">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-success"></div>
                        </div>
                        <div class="contact-person">
                            <p class="mg-b-0 ">Belinda Connor</p>
                            <span>Fort Kent, ME</span>
                        </div>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
                <a href="" class="contact-list-link new">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-success"></div>
                        </div>
                        <div class="contact-person">
                            <p>Britanny Cevallos</p>
                            <span>Shiboygan Falls, WI</span>
                        </div>
                        <span class="tx-info tx-12"><span class="square-8 bg-info rounded-circle"></span> 3 new</span>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
                <a href="" class="contact-list-link new">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-success"></div>
                        </div>
                        <div class="contact-person">
                            <p>Brandon Lawrence</p>
                            <span>Snohomish, WA</span>
                        </div>
                        <span class="tx-info tx-12"><span class="square-8 bg-info rounded-circle"></span> 1 new</span>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
                <a href="" class="contact-list-link">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-success"></div>
                        </div>
                        <div class="contact-person">
                            <p>Andrew Wiggins</p>
                            <span>Springfield, MA</span>
                        </div>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
                <a href="" class="contact-list-link">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-success"></div>
                        </div>
                        <div class="contact-person">
                            <p>Theodore Gristen</p>
                            <span>Nashville, TN</span>
                        </div>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
                <a href="" class="contact-list-link">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-success"></div>
                        </div>
                        <div class="contact-person">
                            <p>Deborah Miner</p>
                            <span>North Shore, CA</span>
                        </div>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
            </div><!-- contact-list -->


            <label class="sidebar-label pd-x-25 mg-t-25">Offline Contacts</label>
            <div class="contact-list pd-x-10">
                <a href="" class="contact-list-link">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-gray-500"></div>
                        </div>
                        <div class="contact-person">
                            <p>Marilyn Tarter</p>
                            <span>Clemson, CA</span>
                        </div>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
                <a href="" class="contact-list-link">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-gray-500"></div>
                        </div>
                        <div class="contact-person">
                            <p>Belinda Connor</p>
                            <span>Fort Kent, ME</span>
                        </div>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
                <a href="" class="contact-list-link">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-gray-500"></div>
                        </div>
                        <div class="contact-person">
                            <p>Britanny Cevallos</p>
                            <span>Shiboygan Falls, WI</span>
                        </div>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
                <a href="" class="contact-list-link">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-gray-500"></div>
                        </div>
                        <div class="contact-person">
                            <p>Brandon Lawrence</p>
                            <span>Snohomish, WA</span>
                        </div>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
                <a href="" class="contact-list-link">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-gray-500"></div>
                        </div>
                        <div class="contact-person">
                            <p>Andrew Wiggins</p>
                            <span>Springfield, MA</span>
                        </div>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
                <a href="" class="contact-list-link">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-gray-500"></div>
                        </div>
                        <div class="contact-person">
                            <p>Theodore Gristen</p>
                            <span>Nashville, TN</span>
                        </div>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
                <a href="" class="contact-list-link">
                    <div class="d-flex">
                        <div class="pos-relative">
                            <img src="https://via.placeholder.com/500" alt="">
                            <div class="contact-status-indicator bg-gray-500"></div>
                        </div>
                        <div class="contact-person">
                            <p>Deborah Miner</p>
                            <span>North Shore, CA</span>
                        </div>
                    </div><!-- d-flex -->
                </a><!-- contact-list-link -->
            </div><!-- contact-list -->

        </div><!-- #contacts -->


        <div class="tab-pane pos-absolute a-0 mg-t-60 attachment-scrollbar" id="attachments" role="tabpanel">
            <label class="sidebar-label pd-x-25 mg-t-25">Recent Attachments</label>
            <div class="media-file-list">
                <div class="media">
                    <div class="pd-10 bg-gray-500 bg-mojito wd-50 ht-60 tx-center d-flex align-items-center justify-content-center">
                        <i class="far fa-file-image tx-28 tx-white"></i>
                    </div>
                    <div class="media-body">
                        <p class="mg-b-0 tx-13">IMG_43445</p>
                        <p class="mg-b-0 tx-12 op-5">JPG Image</p>
                        <p class="mg-b-0 tx-12 op-5">1.2mb</p>
                    </div><!-- media-body -->
                    <a href="" class="more"><i class="icon ion-android-more-vertical tx-18"></i></a>
                </div><!-- media -->
                <div class="media mg-t-20">
                    <div class="pd-10 bg-gray-500 bg-purple wd-50 ht-60 tx-center d-flex align-items-center justify-content-center">
                        <i class="far fa-file-video tx-28 tx-white"></i>
                    </div>
                    <div class="media-body">
                        <p class="mg-b-0 tx-13">VID_6543</p>
                        <p class="mg-b-0 tx-12 op-5">MP4 Video</p>
                        <p class="mg-b-0 tx-12 op-5">24.8mb</p>
                    </div><!-- media-body -->
                    <a href="" class="more"><i class="icon ion-android-more-vertical tx-18"></i></a>
                </div><!-- media -->
                <div class="media mg-t-20">
                    <div class="pd-10 bg-gray-500 bg-reef wd-50 ht-60 tx-center d-flex align-items-center justify-content-center">
                        <i class="far fa-file-word tx-28 tx-white"></i>
                    </div>
                    <div class="media-body">
                        <p class="mg-b-0 tx-13">Tax_Form</p>
                        <p class="mg-b-0 tx-12 op-5">Word Document</p>
                        <p class="mg-b-0 tx-12 op-5">5.5mb</p>
                    </div><!-- media-body -->
                    <a href="" class="more"><i class="icon ion-android-more-vertical tx-18"></i></a>
                </div><!-- media -->
                <div class="media mg-t-20">
                    <div class="pd-10 bg-gray-500 bg-firewatch wd-50 ht-60 tx-center d-flex align-items-center justify-content-center">
                        <i class="far fa-file-pdf tx-28 tx-white"></i>
                    </div>
                    <div class="media-body">
                        <p class="mg-b-0 tx-13">Getting_Started</p>
                        <p class="mg-b-0 tx-12 op-5">PDF Document</p>
                        <p class="mg-b-0 tx-12 op-5">12.7mb</p>
                    </div><!-- media-body -->
                    <a href="" class="more"><i class="icon ion-android-more-vertical tx-18"></i></a>
                </div><!-- media -->
                <div class="media mg-t-20">
                    <div class="pd-10 bg-gray-500 bg-firewatch wd-50 ht-60 tx-center d-flex align-items-center justify-content-center">
                        <i class="far fa-file-pdf tx-28 tx-white"></i>
                    </div>
                    <div class="media-body">
                        <p class="mg-b-0 tx-13">Introduction</p>
                        <p class="mg-b-0 tx-12 op-5">PDF Document</p>
                        <p class="mg-b-0 tx-12 op-5">7.7mb</p>
                    </div><!-- media-body -->
                    <a href="" class="more"><i class="icon ion-android-more-vertical tx-18"></i></a>
                </div><!-- media -->
                <div class="media mg-t-20">
                    <div class="pd-10 bg-gray-500 bg-mojito wd-50 ht-60 tx-center d-flex align-items-center justify-content-center">
                        <i class="far fa-file-image tx-28 tx-white"></i>
                    </div>
                    <div class="media-body">
                        <p class="mg-b-0 tx-13">IMG_43420</p>
                        <p class="mg-b-0 tx-12 op-5">JPG Image</p>
                        <p class="mg-b-0 tx-12 op-5">2.2mb</p>
                    </div><!-- media-body -->
                    <a href="" class="more"><i class="icon ion-android-more-vertical tx-18"></i></a>
                </div><!-- media -->
                <div class="media mg-t-20">
                    <div class="pd-10 bg-gray-500 bg-mojito wd-50 ht-60 tx-center d-flex align-items-center justify-content-center">
                        <i class="far fa-file-image tx-28 tx-white"></i>
                    </div>
                    <div class="media-body">
                        <p class="mg-b-0 tx-13">IMG_43447</p>
                        <p class="mg-b-0 tx-12 op-5">JPG Image</p>
                        <p class="mg-b-0 tx-12 op-5">3.2mb</p>
                    </div><!-- media-body -->
                    <a href="" class="more"><i class="icon ion-android-more-vertical tx-18"></i></a>
                </div><!-- media -->
                <div class="media mg-t-20">
                    <div class="pd-10 bg-gray-500 bg-purple wd-50 ht-60 tx-center d-flex align-items-center justify-content-center">
                        <i class="far fa-file-video tx-28 tx-white"></i>
                    </div>
                    <div class="media-body">
                        <p class="mg-b-0 tx-13">VID_6545</p>
                        <p class="mg-b-0 tx-12 op-5">AVI Video</p>
                        <p class="mg-b-0 tx-12 op-5">14.8mb</p>
                    </div><!-- media-body -->
                    <a href="" class="more"><i class="icon ion-android-more-vertical tx-18"></i></a>
                </div><!-- media -->
                <div class="media mg-t-20">
                    <div class="pd-10 bg-gray-500 bg-reef wd-50 ht-60 tx-center d-flex align-items-center justify-content-center">
                        <i class="far fa-file-word tx-28 tx-white"></i>
                    </div>
                    <div class="media-body">
                        <p class="mg-b-0 tx-13">Secret_Document</p>
                        <p class="mg-b-0 tx-12 op-5">Word Document</p>
                        <p class="mg-b-0 tx-12 op-5">4.5mb</p>
                    </div><!-- media-body -->
                    <a href="" class="more"><i class="icon ion-android-more-vertical tx-18"></i></a>
                </div><!-- media -->
            </div><!-- media-list -->
        </div><!-- #history -->
        <div class="tab-pane pos-absolute a-0 mg-t-60 schedule-scrollbar" id="calendar" role="tabpanel">
            <label class="sidebar-label pd-x-25 mg-t-25">Time &amp; Date</label>
            <div class="pd-x-25">
                <h2 id="brTime" class="br-time"></h2>
                <h6 id="brDate" class="br-date"></h6>
            </div>

            <label class="sidebar-label pd-x-25 mg-t-25">Events Calendar</label>
            <div class="datepicker sidebar-datepicker"></div>


            <label class="sidebar-label pd-x-25 mg-t-25">Event Today</label>
            <div class="pd-x-25">
                <div class="list-group sidebar-event-list mg-b-20">
                    <div class="list-group-item">
                        <div>
                            <h6>Roven's 32th Birthday</h6>
                            <p>2:30PM</p>
                        </div>
                        <a href="" class="more"><i class="icon ion-android-more-vertical tx-18"></i></a>
                    </div><!-- list-group-item -->
                    <div class="list-group-item">
                        <div>
                            <h6>Regular Workout Schedule</h6>
                            <p>7:30PM</p>
                        </div>
                        <a href="" class="more"><i class="icon ion-android-more-vertical tx-18"></i></a>
                    </div><!-- list-group-item -->
                </div><!-- list-group -->

                <a href="" class="btn btn-block btn-outline-secondary tx-uppercase tx-12 tx-spacing-2">+ Add Event</a>
                <br>
            </div>

        </div>
        <div class="tab-pane pos-absolute a-0 mg-t-60 settings-scrollbar" id="settings" role="tabpanel">
            <label class="sidebar-label pd-x-25 mg-t-25">Quick Settings</label>

            <div class="sidebar-settings-item">
                <h6 class="tx-13 tx-normal">Sound Notification</h6>
                <p class="op-5 tx-13">Play an alert sound everytime there is a new notification.</p>
                <div class="br-switchbutton checked">
                    <input type="hidden" name="switch1" value="true">
                    <span></span>
                </div><!-- br-switchbutton -->
            </div>
            <div class="sidebar-settings-item">
                <h6 class="tx-13 tx-normal">2 Steps Verification</h6>
                <p class="op-5 tx-13">Sign in using a two step verification by sending a verification code to your
                    phone.</p>
                <div class="br-switchbutton">
                    <input type="hidden" name="switch2" value="false">
                    <span></span>
                </div><!-- br-switchbutton -->
            </div>
            <div class="sidebar-settings-item">
                <h6 class="tx-13 tx-normal">Location Services</h6>
                <p class="op-5 tx-13">Allowing us to access your location</p>
                <div class="br-switchbutton">
                    <input type="hidden" name="switch3" value="false">
                    <span></span>
                </div><!-- br-switchbutton -->
            </div>
            <div class="sidebar-settings-item">
                <h6 class="tx-13 tx-normal">Newsletter Subscription</h6>
                <p class="op-5 tx-13">Enables you to send us news and updates send straight to your email.</p>
                <div class="br-switchbutton checked">
                    <input type="hidden" name="switch4" value="true">
                    <span></span>
                </div><!-- br-switchbutton -->
            </div>
            <div class="sidebar-settings-item">
                <h6 class="tx-13 tx-normal">Your email</h6>
                <div class="pos-relative">
                    <input title="email" type="email" name="email" class="form-control" value="janedoe@domain.com">
                </div>
            </div>

            <div class="pd-y-20 pd-x-25">
                <h6 class="tx-13 tx-normal tx-white mg-b-20">More Settings</h6>
                <a href="" class="btn btn-block btn-outline-secondary tx-uppercase tx-11 tx-spacing-2">Account
                    Settings</a>
                <a href="" class="btn btn-block btn-outline-secondary tx-uppercase tx-11 tx-spacing-2">Privacy
                    Settings</a>
            </div>
        </div>
    </div><!-- tab-content -->

</div><!-- br-sideright -->
<!-- ########## END: RIGHT PANEL ########## --->

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="<?php echo base_url() ?>">Cooprinsem Facturas</a>
            <a class="breadcrumb-item" href="<?php echo base_url() ?>">Facturas</a>
        </nav>
    </div><!-- br-pageheader -->

    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <h6 class="br-section-label">Informes de trabajos realizados</h6>
            <form id="formulario_itr" method="post">
                <div id="wizard2">
                    <h3>Datos previos</h3>
                    <section>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="select_etapa" class="form-control-label">Etapa: <span class="tx-danger">*</span></label>
                                    <select id="select_etapa" name="etapa" class="form-control" data-placeholder="Choose one" required>
                                        <option label="Elegir etapa" disabled selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="select_sector" class="form-control-label">Sector: <span class="tx-danger">*</span></label>
                                    <select id="select_sector" name="sector" class="form-control js-example-basic-single" data-placeholder="Choose one 2" required>
                                        <option label="Elegir sector" disabled selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="select_lugar" class="form-control-label">Lugar especifico del trabajo: <span class="tx-danger">*</span></label>
                                    <select id="select_lugar" name="lugar" class="form-control" data-placeholder="Choose one" required>
                                        <option label="Elegir el lugar del trabajo" disabled selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="select_origen" class="form-control-label">Origen del trabajo: <span class="tx-danger">*</span></label>
                                    <select id="select_origen" name="origen" class="form-control js-example-basic-single" data-placeholder="Choose one 2" required>
                                        <option label="Elegir origen del trabajo" disabled selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="select_pabellon" class="form-control-label">Pabellones Involucrados: <span class="tx-danger">*</span></label>
                                    <select id="select_pabellon" name="pabellones[]" class="form-control js-example-basic-single" data-placeholder="Choose one" required multiple="multiple">
                                        <option label="Elegir pabellones" disabled selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="select_contratista" class="form-control-label">Contratista (Rut empresa): <span class="tx-danger">*</span></label>
                                    <select id="select_contratista" name="contratista" class="form-control" data-placeholder="Choose one 2" required>
                                        <option label="Choose one" disabled selected></option>
                                        <option value="Firefox">Firefox</option>
                                        <option value="Chrome">Chrome</option>
                                        <option value="Safari">Safari</option>
                                        <option value="Opera">Opera</option>
                                        <option value="Internet Explorer">Internet Explorer</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="tipo_trabajo" class="form-control-label">Tipo de trabajo: <span class="tx-danger">*</span></label>
                                    <select id="tipo_trabajo" name="tipo_trabajo" class="form-control" data-placeholder="Choose one" required>
                                        <option label="Elegir tipo de trabajo" disabled selected></option>
                                        <option value="Programado">Programado</option>
                                        <option value="Emergencia">Emergencia</option>
                                        <option value="Garantía">Garantía</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="descripcion_trabajo" class="form-control-label">Descripción del trabajo:<span class="tx-danger">*</span></label>
                                    <input id="descripcion_trabajo" class="form-control" name="descripcion_trabajo" placeholder="Descripción del trabajo" type="text" required>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="row_trabajo_anterior">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="identificador_trabajo_anterior" class="form-control-label">Identificador de trabajo anterior: <span class="tx-danger">*</span></label>
                                    <select id="identificador_trabajo_anterior" name="identificador_trabajo_anterior" class="form-control" data-placeholder="Choose one" required>
                                        <option label="Elegir identificador" disabled selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6" id="col_orden_compra">
                                <div class="form-group">
                                    <label class="ckbox">
                                        <input id="orden_compra_checkbox" name="tiene_orden_compra" type="checkbox"><span>¿Tiene orden de compra?</span>
                                    </label>
                                    <input id="orden_compra" class="form-control" name="orden_compra" placeholder="Orden de compra" type="text" readonly>
                                </div>
                            </div>
                            <div class="col-6" id="col_numero_ticket">
                                <div class="form-group">
                                    <label class="ckbox">
                                        <input id="numero_ticket_checkbox" type="checkbox" name="tiene_numero_ticket"><span>¿Tiene número de ticket?</span>
                                    </label>
                                    <input id="numero_ticket" class="form-control" name="numero_ticket" placeholder="Número de ticket" type="text" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="otros" class="form-control-label">Otros:</label>
                                    <textarea rows="3" class="form-control" placeholder="Otros / Información adicional" name="otros"></textarea>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>Fechas de proceso</h3>
                    <section>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="detalle_trabajos_fecha_solicitud" class="form-control-label">Fecha de solicitud de trabajo: <span class="tx-danger">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="icon ion-calendar tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div>
                                        <input id="detalle_trabajos_fecha_solicitud" name="fecha_solicitud" type="text" class="form-control fc-datepicker" placeholder="dd/mm/yyyy" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="detalle_trabajos_fecha_inicio" class="form-control-label">Fecha de inicio:<span class="tx-danger">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="icon ion-calendar tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div>
                                        <input id="detalle_trabajos_fecha_inicio" name="fecha_inicio" type="text" class="form-control fc-datepicker" placeholder="dd/mm/yyyy" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="detalle_trabajos_fecha_fin" class="form-control-label">Fecha de término:<span class="tx-danger">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="icon ion-calendar tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div>
                                        <input id="detalle_trabajos_fecha_fin" name="fecha_fin" type="text" class="form-control fc-datepicker" placeholder="dd/mm/yyyy" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="detalle_trabajos_hora_solicitud" class="form-control-label">Hora de solicitud de trabajo: <span class="tx-danger">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="icon ion-clock tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div>
                                        <input id="detalle_trabajos_hora_solicitud" name="hora_solicitud" class="form-control mascara_tiempo" placeholder="hh:mm" type="text" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="detalle_trabajos_hora_inicio" class="form-control-label">Hora de inicio de trabajo: <span class="tx-danger">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="icon ion-clock tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div>
                                        <input id="detalle_trabajos_hora_inicio" name="hora_inicio" class="form-control mascara_tiempo" placeholder="hh:mm" type="text" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="detalle_trabajos_hora_fin" class="form-control-label">Hora de fin de trabajo: <span class="tx-danger">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="icon ion-clock tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div>
                                        <input id="detalle_trabajos_hora_fin" name="hora_fin" class="form-control mascara_tiempo" placeholder="hh:mm" type="text" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>Recursos utilizados</h3>
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="tipo_maquinaria" class="form-control-label">Tipo de maquinaria:</label>
                                    <input id="tipo_maquinaria" class="form-control" name="tipo_maquinaria" placeholder="Tipo de maquinaria" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="horas_maquinaria" class="form-control-label">Número de horas maquina:</label>
                                    <input id="horas_maquinaria" class="form-control" name="horas_maquinaria" placeholder="Número de horas maquina" type="text">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="numero_personas" class="form-control-label">Número de personas: <span class="tx-danger">*</span></label>
                                    <input id="numero_personas" class="form-control" name="numero_personas" placeholder="Número de personas" type="text" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="detalle_trabajo_realizado" class="form-control-label">Detalle del trabajo realizado, reparaciones, aprobaciones y rechazos: <span class="tx-danger">*</span></label>
                                    <textarea id="detalle_trabajo_realizado" name="detalle_trabajo_realizado" rows="3" class="form-control" placeholder="" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <label class="ckbox">
                                    <input type="checkbox" id="generacion_residuos" name="generacion_residuos"><span>Generación de residuos para retiro</span>
                                </label>
                            </div>
                            <div class="col-6" id="col_contratista_retira_residuos">
                                <label class="ckbox">
                                    <input type="checkbox" id="contratista_retira_residuos" name="contratista_retira_residuos"><span>Contratista retira residuos</span>
                                </label>
                            </div>
                        </div>
                        <div class="row" id="row_residuos_generados">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="residuos_generados" class="form-control-label">Indicar los residuos generados: <span class="tx-danger">*</span></label>
                                    <textarea id="residuos_generados" name="residuos_generados" rows="5" class="form-control" placeholder=""></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="form-control-label"><b>Nota</b>: Todos los residuos generados en las mantenciones, deben ser segrados en la unidad de transferencia para su posterior gestión con el SGA.</label>
                            </div>
                        </div>
                    </section>
                    <h3>Responsables de informe</h3>
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="responsable_solicitud" class="form-control-label">Responsable de la solicitud:</label>
                                    <input id="responsable_solicitud" class="form-control" name="responsable_solicitud" placeholder="Nombre y apellido" type="text" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="responsable_recepcion" class="form-control-label">Responsable de recepción de trabajo:</label>
                                    <input id="responsable_recepcion" class="form-control" name="responsable_recepcion" placeholder="Nombre y apellido" type="text" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="responsable_empresa" class="form-control-label">Empresa responsable contratista:</label>
                                    <input id="responsable_empresa" class="form-control" name="responsable_empresa" placeholder="Nombre y apellido" type="text" required readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="responsable_contratista" class="form-control-label">Nombre del responsable contratista:</label>
                                    <input id="responsable_contratista" class="form-control" name="responsable_contratista" placeholder="Nombre y apellido" type="text" required>
                                </div>
                            </div>
                        </div>
                        <div class="row btn_descarga">
                            <div class="col-3 offset-9" style="text-align: center;">
                                <button class="btn btn-warning btn-block mg-b-10">Descargar PDF</button>
                            </div>
                        </div>
                    </section>
                </div>
            </form>
        </div><!-- br-section-wrapper -->

    </div><!-- br-pagebody -->
    <footer class="br-footer">
        <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2019. Cooprinsem. Todos los derechos reservados.</div>
        </div>
        <div class="footer-right d-flex align-items-center">
        </div>
    </footer>
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<script src="<?php echo base_url('assets/lib/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/jquery-ui/ui/widgets/datepicker.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/perfect-scrollbar/perfect-scrollbar.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/peity/jquery.peity.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/highlightjs/highlight.pack.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/jquery-steps/build/jquery.steps.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/parsleyjs/parsley.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/select2/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/timepicker/jquery.timepicker.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/spectrum-colorpicker/spectrum.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/jquery.maskedinput/jquery.maskedinput.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/bootstrap-tagsinput/bootstrap-tagsinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/lib/ion-rangeslider/js/ion.rangeSlider.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/bracket.js') ?>"></script>

<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

<script>
    $(window).on('load', function () { // makes sure the whole site is loaded
        $('#status').fadeOut('fast'); // will first fade out the loading animation
        $('#preloader').fadeOut('fast'); // will fade out the white DIV that covers the website.
    })
</script>

<script>
    $(function () {
        'use strict';

        $('#wizard2').steps({
            labels: {
                previous: "Anterior",
                next: "Siguiente",
                finish: "Enviar",
            },
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            onInit: function (event, currentIndex) {
                $('#select_etapa').select2({
                    placeholder: "Elegir etapa"
                });
                $('#select_pabellon').select2({
                    placeholder: "Elegir pabellon"
                });
                $('#select_sector').select2({
                    placeholder: "Elegir sector"
                });
                $('#select_lugar').select2({
                    placeholder: "Elegir el lugar del trabajo"
                });
                $('#select_origen').select2({
                    placeholder: "Elegir origen del trabajo"
                });
                $('#select_contratista').select2({
                    placeholder: "Elegir contratista"
                });
                $('#tipo_trabajo').select2({
                    placeholder: "Elegir contratista"
                });
            },
            onStepChanging: function (event, currentIndex, newIndex) {
                if (currentIndex < newIndex) {
                    if (currentIndex === 0) {
                        // let select_etapa = $('#select_etapa').parsley();
                        // let select_sector = $('#select_sector').parsley();
                        // let descripcion = $('#descripcion_trabajo').parsley();
                        // let select_lugar = $('#select_lugar').parsley();
                        // let select_origen = $('#select_origen').parsley();
                        // let select_contratista = $('#select_contratista').parsley();
                        // let select_tipo_trabajo = $('#tipo_trabajo').parsley();
                        //
                        // if (select_etapa.isValid() && descripcion.isValid() && select_sector.isValid() && select_lugar.isValid() && select_origen.isValid() && select_contratista.isValid() && select_tipo_trabajo.isValid()) {
                        //     return true;
                        // }
                        // else {
                        //     select_etapa.validate();
                        //     select_sector.validate();
                        //     descripcion.validate();
                        //     select_lugar.validate();
                        //     select_origen.validate();
                        //     select_contratista.validate();
                        //     select_tipo_trabajo.validate();
                        //     return false;
                        // }
                        return true;
                    }

                    // Step 2 form validation
                    if (currentIndex === 1) {

                        // let fecha_solicitud = $('#detalle_trabajos_fecha_solicitud');
                        // let hora_solicitud = $('#detalle_trabajos_hora_solicitud');
                        // let detalle_trabajos_fecha_solicitud = fecha_solicitud.val();
                        // //let detalle_trabajos_fecha_solicitud = fecha_solicitud.val().split('-')[2] + '-' + fecha_solicitud.val().split('-')[1] + '-' + fecha_solicitud.val().split('-')[0];
                        // let detalle_trabajos_hora_solicitud = hora_solicitud.val();
                        // let combinado_momento_solicitud = detalle_trabajos_fecha_solicitud + ' ' + detalle_trabajos_hora_solicitud+":00";
                        // //console.log('combinado_momento_solicitud: ', combinado_momento_solicitud);
                        // let date_momento_solicitud = new Date(combinado_momento_solicitud);
                        // let momento_solicitud = date_momento_solicitud.getTime();
                        // //console.log('momento_solicitud: ', momento_solicitud);
                        // if(isNaN(momento_solicitud)) {
                        //     combinado_momento_solicitud = detalle_trabajos_fecha_solicitud + 'T' + detalle_trabajos_hora_solicitud+":00";
                        //     console.log('combinado_momento_solicitudcc MAC: ', combinado_momento_solicitud);
                        //     date_momento_solicitud = new Date(combinado_momento_solicitud);
                        //     momento_solicitud = date_momento_solicitud.getTime();
                        //     console.log('momento_solicitud MAC: ', momento_solicitud);
                        // }
                        //
                        // let fecha_inicio = $('#detalle_trabajos_fecha_inicio');
                        // let hora_inicio = $('#detalle_trabajos_hora_inicio');
                        // let detalle_trabajos_fecha_inicio = fecha_inicio.val();
                        // //let detalle_trabajos_fecha_inicio = fecha_inicio.val().split('-')[2] + '-' + fecha_inicio.val().split('-')[1] + '-' + fecha_inicio.val().split('-')[0];
                        // let detalle_trabajos_hora_inicio = hora_inicio.val();
                        // let combinado_momento_inicio = detalle_trabajos_fecha_inicio + ' ' + detalle_trabajos_hora_inicio+":00";
                        // //console.log('combinado_momento_inicio MAC: ', combinado_momento_inicio);
                        // let date_momento_inicio = new Date(combinado_momento_inicio);
                        // let momento_inicio = date_momento_inicio.getTime();
                        // //console.log('momento_inicio: ', momento_inicio);
                        // if(isNaN(momento_inicio)) {
                        //     combinado_momento_inicio = detalle_trabajos_fecha_inicio + 'T' + detalle_trabajos_hora_inicio+":00";
                        //     console.log('combinado_momento_inicio: ', combinado_momento_inicio);
                        //     date_momento_inicio = new Date(combinado_momento_inicio);
                        //     momento_inicio = date_momento_inicio.getTime();
                        //     console.log('momento_inicio MAC:', momento_inicio);
                        // }
                        //
                        // let fecha_fin = $('#detalle_trabajos_fecha_fin');
                        // let hora_fin = $('#detalle_trabajos_hora_fin');
                        // let detalle_trabajos_fecha_fin = fecha_fin.val();
                        // //let detalle_trabajos_fecha_fin = fecha_fin.val().split('-')[2] + '-' + fecha_fin.val().split('-')[1] + '-' + fecha_fin.val().split('-')[0];
                        // let detalle_trabajos_hora_fin = hora_fin.val();
                        // let combinado_momento_fin = detalle_trabajos_fecha_fin + ' ' + detalle_trabajos_hora_fin+":00";
                        // let date_momento_fin = new Date(combinado_momento_fin);
                        // let momento_fin = date_momento_fin.getTime();
                        // //console.log('momento_fin: ', momento_fin);
                        // if(isNaN(momento_fin)) {
                        //     combinado_momento_fin = detalle_trabajos_fecha_fin + 'T' + detalle_trabajos_hora_fin+":00";
                        //     console.log('combinado_momento_fin MAC: ', combinado_momento_fin);
                        //     date_momento_fin = new Date(combinado_momento_fin);
                        //     momento_fin = date_momento_fin.getTime();
                        //     console.log('momento_fin MAC:', momento_fin);
                        // }
                        //
                        // let valida_fecha_solicitud = fecha_solicitud.parsley();
                        // let valida_hora_solicitud = hora_solicitud.parsley();
                        // let valida_fecha_inicio = fecha_inicio.parsley();
                        // let valida_hora_inicio = hora_inicio.parsley();
                        // let valida_fecha_fin = fecha_fin.parsley();
                        // let valida_hora_fin = hora_fin.parsley();
                        //
                        // // console.log(momento_solicitud - momento_inicio);
                        // // console.log(momento_inicio - momento_fin);
                        //
                        // console.log(momento_solicitud);
                        // console.log(momento_inicio);
                        // console.log(momento_fin);
                        //
                        //
                        // if (valida_fecha_solicitud.isValid() && valida_hora_solicitud.isValid() && valida_fecha_inicio.isValid() && valida_hora_inicio.isValid() && valida_fecha_fin.isValid() && valida_hora_fin.isValid()) {
                        //     if (momento_solicitud < momento_inicio && momento_inicio < momento_fin) {
                        //         return true;
                        //     }
                        //     else {
                        //         alert('las fechas deben estar en orden cronológico');
                        //         return false;
                        //     }
                        // }
                        // else {
                        //     valida_fecha_solicitud.validate();
                        //     valida_hora_solicitud.validate();
                        //     valida_fecha_inicio.validate();
                        //     valida_hora_inicio.validate();
                        //     valida_fecha_fin.validate();
                        //     valida_hora_fin.validate();
                        //     return false;
                        // }
                        return true
                    }
                    if (currentIndex === 2) {

                        // let numero_personas = $('#numero_personas').parsley();
                        // let detalle_trabajo_realizado = $('#detalle_trabajo_realizado').parsley();
                        //
                        // let generacion_residuos = jQuery('#generacion_residuos');
                        //
                        // if (generacion_residuos.is(':checked')) {
                        //     let residuos_generados = $('#residuos_generados').parsley();
                        //     if (numero_personas.isValid() && detalle_trabajo_realizado.isValid() && residuos_generados.isValid()) {
                        //         return true;
                        //     }
                        //     else {
                        //         numero_personas.validate();
                        //         detalle_trabajo_realizado.validate();
                        //         residuos_generados.validate();
                        //         return false;
                        //     }
                        // }
                        // else {
                        //     if (numero_personas.isValid() && detalle_trabajo_realizado.isValid()) {
                        //         return true;
                        //     }
                        //     else {
                        //         numero_personas.validate();
                        //         detalle_trabajo_realizado.validate();
                        //         return false;
                        //     }
                        // }
                        return true
                    }
                    // Always allow step back to the previous step even if the current step is not valid.
                }
                else {
                    return true;
                }
            },
            onFinishing: function (event, currentIndex) {
                let responsable_solicitud = $('#responsable_solicitud').parsley();
                let responsable_recepcion = $('#responsable_recepcion').parsley();
                let responsable_contratista = $('#responsable_contratista').parsley();
                let responsable_empresa = $('#responsable_empresa').parsley();
                if (responsable_solicitud.isValid() && responsable_recepcion.isValid() && responsable_contratista.isValid() && responsable_empresa.isValid()) {
                    $('body').css('cursor', 'progress');
                    let url = '<?php echo base_url('inicio/itr_post') ?>';
                    let postData = $('#formulario_itr').serialize();

                    $.post(url, postData, function (o) {
                        console.log('o: ', o);
                        if (o.result === 1) {
                            $('body').css('cursor', 'initial');
                            $('.btn_descarga').css("display", "block");
                        }
                        else {
                            alert('Itr fallido');
                        }
                    }, 'json');

                    return true;
                }
                else {
                    responsable_solicitud.validate();
                    responsable_recepcion.validate();
                    responsable_contratista.validate();
                    responsable_empresa.validate();
                    return false;
                }
            }
        });


        // Toggles
        $('.br-toggle').on('click', function (e) {
            e.preventDefault();
            $(this).toggleClass('on');
        });

        // Input Masks
        $('#dateMask').mask('99/99/9999');
        $('#phoneMask').mask('(999) 999-9999');
        $('#ssnMask').mask('999-99-9999');

        // Datepicker
        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            maxDate: '0',
            dateFormat: 'yy-mm-dd'
        });

        $('#datepickerNoOfMonths').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            numberOfMonths: 2,
            dateFormat: 'yy-mm-dd'
        });

        $('#setTimeButton').on('click', function () {

        });

        // Color picker
        $('#colorpicker').spectrum({
            color: '#17A2B8'
        });

        $('#showAlpha').spectrum({
            color: 'rgba(23,162,184,0.5)',
            showAlpha: true
        });

        $('#showPaletteOnly').spectrum({
            showPaletteOnly: true,
            showPalette: true,
            color: '#DC3545',
            palette: [
                ['#1D2939', '#fff', '#0866C6', '#23BF08', '#F49917'],
                ['#DC3545', '#17A2B8', '#6610F2', '#fa1e81', '#72e7a6']
            ]
        });


        // Rangeslider
        if ($().ionRangeSlider) {
            $('#rangeslider1').ionRangeSlider();

            $('#rangeslider2').ionRangeSlider({
                min: 100,
                max: 1000,
                from: 550
            });

            $('#rangeslider3').ionRangeSlider({
                type: 'double',
                grid: true,
                min: 0,
                max: 1000,
                from: 200,
                to: 800,
                prefix: '$'
            });

            $('#rangeslider4').ionRangeSlider({
                type: 'double',
                grid: true,
                min: -1000,
                max: 1000,
                from: -500,
                to: 500,
                step: 250
            });
        }

        $('input.mascara_tiempo').inputmask(
            "hh:mm", {
                placeholder: "hh:mm",
                insertMode: true,
                showMaskOnHover: true
            }
        );

    });
</script>

<script>
    let etapas = [];
    $.get("https://agrosuper.tecnoandina.cl/agrosuper/itr/etapa", function (etapas_response) {
        etapas = etapas_response;
        console.log('etapas: ', etapas);
        let etapas_options = "";
        for (let i = 0; i < etapas.length; i++) {
            etapas_options += "<option value='" + etapas[i]['etapa'] + "'>" + etapas[i]['etapa'] + "</option>"
        }
        jQuery('#select_etapa').append(etapas_options)
    });

    let sectores = [];
    $(document).on('change', '#select_etapa', function () {
        for (let i = 0; i < etapas.length; i++) {
            if (etapas[i]['etapa'] === this.value) {
                sectores = etapas[i]['sectores'];
                let sectores_options = "<option value='' selected disabled>Elegir sector</option>";
                for (let k = 0; k < sectores.length; k++) {
                    sectores_options += "<option value='" + sectores[k]['nombre'] + "'>" + sectores[k]['nombre'] + "</option>";
                }
                $('#select_sector').html(sectores_options)
            }
        }
    });

    let pabellones_disponibles_array = [];
    let pabellones_options = "";
    jQuery(document).on('change', '#select_sector', function () {
        for (let i = 0; i < sectores.length; i++) {
            if (sectores[i]['nombre'] === this.value) {
                let cantidad_pabellones = sectores[i]['pabellones'];
                console.log('cantidad_pabellones: ', cantidad_pabellones);
                pabellones_options = "<option id='todos_los_pabellones' value='todos'>Todos los pabellones</option>";
                for (let k = 1; k <= cantidad_pabellones; k++) {
                    pabellones_options += "<option value='" + k + "'> Pabellón " + k + "</option>";
                    pabellones_disponibles_array.push(k.toString());
                }
                jQuery('#select_pabellon').html(pabellones_options);

                console.log('sectores[i]: ', sectores[i]);

                let disabled_option_lugar = "<option label='Elegir el lugar del trabajo' disabled selected></option>"
                let interior_option = "<option value='Interior'>Interior</option>";
                let exterior_option = "<option value='Exterior'>Exterior</option>";

                if (sectores[i]['tipo'] === "Ambas") {
                    jQuery('#select_lugar').html(disabled_option_lugar + interior_option + exterior_option);
                    console.log('Ambas');
                }
                if (sectores[i]['tipo'] === "Interior") {
                    jQuery('#select_lugar').html(disabled_option_lugar + interior_option);
                    console.log('interior');
                }
                if (sectores[i]['tipo'] === "Exterior") {
                    jQuery('#select_lugar').html(disabled_option_lugar + exterior_option);
                }

            }
        }
    });
</script>

<script>
    jQuery(document).on('change', '#select_sector', function () {

    });
</script>

<script>
    let origenes = [];
    jQuery.get("https://agrosuper.tecnoandina.cl/agrosuper/itr/origen", function (origenes_response) {
        origenes = origenes_response;
        //console.log('origenes: ', origenes);
    });

    jQuery(document).on('change', '#select_lugar', function () {
        let origenes_options = "";
        for (let i = 0; i < origenes.length; i++) {
            for (let k = 0; k < origenes[i]['origenes'].length; k++) {
                if (origenes[i]['origenes'][k]['tipo'] === this.value) {
                    origenes_options += "<option value='" + origenes[i]['origenes'][k]['nombre_origen'] + "'>" + origenes[i]['origenes'][k]['nombre_origen'] + "</option>";
                }
            }
        }
        if(this.value === "Exterior") {
            jQuery('#select_pabellon')
                .html("<option value='' selected disabled='disabled'>No aplica</option>")
                .val('')
                .select2();
        }
        else {
            jQuery('#select_pabellon').html(pabellones_options)
        }
        jQuery('#select_origen').append(origenes_options);


    });
</script>

<script>
    let contratistas = [];
    jQuery.get("https://agrosuper.tecnoandina.cl/agrosuper/itr/contratista", function (contratistas_response) {
        contratistas = contratistas_response;
        let contratistas_options = "<option value='' selected disabled>Elegir contratista</option>";
        for (let i = 0; i < contratistas.length; i++) {
            contratistas_options += "<option value='" + contratistas[i]['rut'] + "'>" + contratistas[i]['nombre_display'] + " - " + contratistas[i]['rut'] + "</option>"
        }
        jQuery('#select_contratista').html(contratistas_options);
    });
</script>

<script>
    jQuery(document).on('change', '#select_pabellon', function () {
        console.log('pabellon elegido');
        console.log(jQuery(this).val());
        let eleccion_pabellon = jQuery(this).find('option:selected').val();
        if (eleccion_pabellon === 'todos') {
            jQuery(this).val(pabellones_disponibles_array);
            jQuery(this).trigger("change");
            jQuery('#select_pabellon').select2();
        }
        //let pabellones = jQuery(this).val();
    });
</script>

<script>
    jQuery(document).on('change', '#tipo_trabajo', function () {
        let tipo_trabajo = jQuery(this).val();
        let col_orden_compra = $('#col_orden_compra');
        let col_numero_ticket = $('#col_numero_ticket');
        let row_trabajo_anterior = $('#row_trabajo_anterior');

        col_orden_compra.css('display', 'none');
        col_numero_ticket.css('display', 'none');
        row_trabajo_anterior.css('display', 'none');

        if (tipo_trabajo === "Programado") {
            col_orden_compra.css('display', 'block');
        }
        if (tipo_trabajo === "Emergencia") {
            col_numero_ticket.css('display', 'block');
        }
        if (tipo_trabajo === "Garantía") {

            $('#status').fadeIn('faset');
            $('#preloader').fadeIn('faset', function () {
                col_orden_compra.css('display', 'block');
                col_numero_ticket.css('display', 'block');
                row_trabajo_anterior.css('display', 'block');
            });

            jQuery.get("<?php echo base_url('index.php/inicio/get_itrs_for_garantia') ?>", function (itrs_anteriores) {
                let itrs_anteriores_array = JSON.parse(itrs_anteriores);
                console.log('itrs_anteriores_array: ', itrs_anteriores_array);
                let itrs_anteriores_options = "";
                for (let i = 0; i < itrs_anteriores_array.length; i++) {
                    itrs_anteriores_options += "<option value='" + itrs_anteriores_array[i]['identificador'] + "'>" + itrs_anteriores_array[i]['identificador'] + ' - ' + itrs_anteriores_array[i]['sector'] + "</option>";
                }
                $('#identificador_trabajo_anterior').html("<option label='Elegir identificador' disabled selected></option>").append(itrs_anteriores_options);
                $('#status').fadeOut('fast');
                $('#preloader').fadeOut('fast');
            });
        }
    })
</script>

<script>
    $(document).on('change', '#numero_ticket_checkbox', function () {
        if (this.checked) {
            $('#numero_ticket').attr({'readonly': false, required: true});
        }
        else {
            $('#numero_ticket').attr({'readonly': true, required: false});
        }
    });

    $(document).on('change', '#orden_compra_checkbox', function () {
        if (this.checked) {
            $('#orden_compra').attr({'readonly': false, required: true});
        }
        else {
            $('#orden_compra').attr({'readonly': true, required: false});
        }
    });
</script>

<script>
    $(document).on('change', '#generacion_residuos', function () {
        if (this.checked) {
            $('#col_contratista_retira_residuos').css('display', 'block');
            $('#row_residuos_generados').css('display', 'block');
            $('#residuos_generados').attr({required: true});
        }
        else {
            console.log('NO genera residuos');
            $('#col_contratista_retira_residuos').css('display', 'none');
            $('#row_residuos_generados').css('display', 'none');
            $('#residuos_generados').attr({required: false});
        }
    })
</script>

<script>
    $(document).on('change', '#select_contratista', function () {
        $('#responsable_empresa').val(jQuery(this).find("option:selected").text());
    })
</script>

<script>
    let fecha = '2019-01-02';
    let hora = '01:00';
    let combinado = fecha + ' ' + hora+":00";
    let date = new Date(combinado);

    if(isNaN(date)) {
        //console.log('Safari');
        console.log('timestamp: ', new Date(combinado.replace(' ', 'T')).getTime());
    }
    else {
        console.log('timestamp: ', date.getTime());

    }

    //console.log(new Date().getTime())

</script>

</body>
</html>
